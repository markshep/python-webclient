# This creates an object for interacting with a web server which keeps state
# (such as cookies) and also glosses over many of the gnarly details of the
# abysmal HTTP support in Python's standard modules.  Since writing this I've
# discovered the "requests" module (http://python-requests.org/), which sounds
# much better, so this should probably be refactored to use that instead.

import base64
import gzip
import httplib
import mimetypes
import re
import socket
import StringIO
import sys
import urllib
import urlparse


content_type_re = re.compile(r'^Content-Type:\s*(.*?)(?:;(.*))\r\n$', re.I)


class MimeException(Exception):

    def __init__(self, message, mimetype, headers, content):
        Exception.__init__(self, message)
        self.mimetype = mimetype
        self.headers = headers
        self.content = content



class HttpBadStatus(Exception):

    def __init__(self, message, status, headers, content):
        Exception.__init__(self, message)
        self.status = status
        self.headers = headers
        self.content = content
        self.mimetype = None
        for header in headers:
            match = content_type_re.match(header)
            if match:
                self.mimetype = match.groups()


class HttpRedirect(HttpBadStatus):

    def __init__(self, message, status, location, headers, content):
        HttpBadStatus.__init__(self, message, status, headers, content)
        self.location = location


class HttpClientError(HttpBadStatus):
    pass


class HttpForbiddenError(HttpClientError):
    pass


class HttpServerError(HttpBadStatus):
    pass



class WebClient():


    def __init__(self, base_url, username=None, password=None):
        if (username is not None) and (password is not None):
            # shove the username & password into the base URL, which
            # is then picked up later to use HTTP basic authentication
            bits = urlparse.urlparse(base_url)
            port = ((bits.port is not None) and ':%d' % (bits.port,)) or ''
            base_url = '%s://%s:%s@%s%s%s' % (bits.scheme, username, password, bits.hostname, port, bits.path)
        self.base_url = base_url
        self.user_agent = 'python-webclient/1.0 (https://bitbucket.org/markshep/python-webclient)'
        self.timeout = 30
        self.retries = 10
        self.backoff = 0

        # state
        self.cookies = {}
        self.logging = False
        self.print_request_headers = False
        self.print_response_headers = False


    def log(self, message):
        if self.logging:
            print >>sys.stderr, message


    def do_request(self, path, postdata=None, mimetype='text/html', attachment=False, dump=False,
                   extra_headers={}, force_charset=None, expected_status=200, verb=None, timeout=30):
        headers = {
            'User-Agent': self.user_agent,
            'Accept-Encoding': 'gzip',
            }
        headers.update(extra_headers)
        headers['Cookie'] = '; '.join(map('='.join, self.cookies.items()))
        if path.startswith('http://') or path.startswith('https://'):
            url = path
        else:
            url = self.base_url + path
        bits = urlparse.urlparse(url)
        if (bits.username != None) and (bits.password != None):
            # basic authentication with username/password embedded in the URL
            encoded = base64.b64encode('%s:%s' % (bits.username, bits.password,))
            headers['Authorization'] = 'Basic %s' % (encoded,)
        if bits.scheme == 'https':
            conn = httplib.HTTPSConnection(bits.hostname, bits.port, timeout=timeout)
        else:
            conn = httplib.HTTPConnection(bits.hostname, bits.port, timeout=timeout)
        path = bits.path
        if bits.query:
            path += '?%s' % (bits.query,)

        # make the actual request using the appropriate HTTP method
        if verb is None:
            verb = ((postdata is None) and 'GET') or 'POST'

        if postdata is None:
            self.log('%s %s' % (verb, path,))
            if self.print_request_headers:
                print ' Request headers:'
                for header, value, in headers.items():
                    print '  %s: %s' % (header, value,)
            conn.request(verb, path, headers=headers)
        else:
            if type(postdata) is dict:
                # generate a simple single MIME entity for these key/value pairs
                mime_type = 'application/x-www-form-urlencoded'
                # urllib is doesn't handle unicode values, so we have to do it ourselves
                utf8_postdata = {}
                for key, value, in postdata.items():
                    if type(value) is unicode:
                        utf8_postdata[key] = value.encode('utf-8')
                    else:
                        # it's a byte string already
                        utf8_postdata[key] = value
                body = urllib.urlencode(utf8_postdata)
            else:
                # a pre-generated MIME entity - should be (content-type, body,)
                assert(type(postdata) is tuple)
                assert(len(postdata) == 2)
                mime_type, body, = postdata
                if type(body) is unicode:
                    # hmm, we don't actually say we're sending utf-8, but then neither does a
                    # real browser - maybe 'multipart/form-data' is implicitly utf-8?
                    body = body.encode('utf-8')
            size = len(body)
            headers['Content-Type'] = mime_type
            headers['Content-Length'] = size
            self.log('%s sending %d byte(s) to %s (%s)' % (verb, size, path, mime_type,))
            if self.print_request_headers:
                print ' Request headers:'
                for header, value, in headers.items():
                    print '  %s: %s' % (header, value,)
            conn.request(verb, path, body, headers)

        # get response and remember all cookies
        response = conn.getresponse()
        headers = response.msg.headers
        if self.print_response_headers:
            print ' Response headers:'
            for header in headers:
                print '  %s' % (header.rstrip('\r\n'),)
        for cookie in response.msg.getallmatchingheaders('set-cookie'):
            header = cookie.split(': ', 1)[1]
            key, content, = header.split('=', 1)
            value = content.split(';')[0]
            self.cookies[key] = value
            self.log('Set-Cookie: %s = %s' % (key, value,))
        content = response.read()

        for header in headers:
            if header == 'Content-Encoding: gzip\r\n':
                content = gzip.GzipFile(fileobj=StringIO.StringIO(content)).read()

        # check response
        version, status, reason, = response.version, response.status, response.reason
        result = 'Got "HTTP/%d.%d %d %s" for %s' % (version / 10, version % 10, status, reason, url,)
        if (status >= 300) and (status < 400):
            # redirects
            location = response.getheader('Location')
            self.log('Redirect: %d %s' % (status,location,))
            raise HttpRedirect(result, status, location, headers, content)
        elif (status >= 400) and (status < 500):
            if status == 403:
                self.log('Forbidden: %d' % (status,))
                raise HttpForbiddenError(result, status, headers, content)
            else:
                self.log('Client Error: %d' % (status,))
                raise HttpClientError(result, status, headers, content)
        elif (status >= 500) and (status < 600):
            self.log('Server Error: %d' % (status,))
            raise HttpServerError(result, status, headers, content)
        elif status != expected_status:
            # some other error
            raise Exception('Failed to %s %s: %s' % (verb, url, result,))

        # 200 OK
        bits = response.getheader('content-type').split(';')
        if bits[0] != mimetype:
            message = 'Wrong MIME type - got "%s", not "%s"' % (bits[0], mimetype,)
            raise MimeException(message, bits[0], headers, content)

        # decode text content from whatever the supplied encoding is to a Python unicode string
        if bits[0].startswith('text/'):
            charset = None
            for bit in bits[1:]:
                name, value, = bit.lstrip(' ').split('=', 1)
                if name == 'charset':
                    charset = value
            if charset is None:
                if force_charset is None:
                    self.log('Warning: missing charset - assuming utf-8')
                    charset = 'utf-8'
                else:
                    charset = force_charset
            elif force_charset is not None:
                self.log('Warning: overriding charset from %s to %s' % (charset, force_charset,))
                charset = force_charset
            content = content.decode(charset)

        if dump:
            for (header, value,) in response.getheaders():
                self.log('%s: %s' % (header, value,))
            self.log('')
            self.log(content)

        if attachment:
            disposition = response.getheader('content-disposition')
            bits = disposition.split(';')
            if bits[0] != 'attachment':
                raise Exception('Expected attachment, but got content disposition of "%s"' % (bits[0],))
            for bit in bits[1:]:
                name, value, = bit.lstrip(' ').split('=', 1)
                if name == 'filename':
                    return content, value,
            raise Exception('Attachment filename not found')
        else:
            return content


    def multipart_post_data(self, fields=[], files=[]):
        # Hmmmmmm, httplib is a bit crap and doesn't support sending POST data as
        # "multipart/form-data".  See http://bugs.python.org/issue3244 for details. This
        # method is a helper to achive that.  The code is lifted from
        # http://code.activestate.com/recipes/146306-http-client-to-post-using-multipartform-data/
        '''
        fields is a sequence of (name, value) elements for regular form fields.
        files is a sequence of (name, filename, value) elements for data to be uploaded as files
        Return (content_type, body) ready for httplib.HTTP instance
        '''
        boundary = u'----------ThIs_Is_tHe_bouNdaRY_$'
        lines = []
        for key, value, in fields:
            lines.append(u'--' + boundary)
            lines.append(u'Content-Disposition: form-data; name="%s"' % (key,))
            lines.append(u'')
            lines.append(value)
        for key, filename, value, in files:
            lines.append(u'--' + boundary)
            lines.append(u'Content-Disposition: form-data; name="%s"; filename="%s"' % (key, filename,))
            mime_type = mimetypes.guess_type(filename)[0] or u'application/octet-stream'
            lines.append(u'Content-Type: %s' % (mime_type,))
            lines.append(u'')
            lines.append(value)
        lines.append(u'--%s--' % (boundary,))
        lines.append(u'')
        body = u'\r\n'.join(lines)
        content_type = u'multipart/form-data; boundary=%s' % (boundary,)
        return content_type, body,


    def request(self, path, postdata=None, mimetype='text/html', attachment=False, dump=False,
                extra_headers={}, force_charset=None, expected_status=200, verb=None):
        failures = []
        timeout = self.timeout
        for attempt in range(0, self.retries):
            try:
                return self.do_request(path, postdata, mimetype, attachment, dump, extra_headers,
                                       force_charset, expected_status, verb, timeout)
            except socket.timeout, e:
                failure = 'timed out after %d second(s)' % (timeout,)
            except socket.error, e:
                failure = '%s: %s' % (e.__class__.__name__, e,)
            self.log(failure)
            failures.append(failure)
            timeout += self.backoff
        else:
            raise Exception('Giving up after %d attempt(s) to get "%s" from %s %s' % (self.retries, path, self.base_url, failures,))


    def make_path(self, url):
        # turns a full URL into a path releative to base_url
        if url.startswith(self.base_url):
            # full URL including protocol & hostname
            return url[len(self.base_url):]
        elif url.startswith('/'):
            # URL relative to the server
            stem = urlparse.urlparse(self.base_url).path
            if not url.startswith(stem):
                raise Exception('URL "%s" doesn\'t start with "%s"' % (url, stem,))
            return url[len(stem):]
        else:
            # try to make a relative URL using "../"
            base_bits = urlparse.urlparse(self.base_url)
            bits = urlparse.urlparse(url)
            if (bits.scheme == base_bits.scheme) and (bits.netloc == base_bits.netloc):
                base_dirs = list(base_bits.path[1:].split('/')[:-1])
                elements = list(bits.path[1:].split('/'))
                while base_dirs[0] == elements[0]:
                    base_dirs.pop(0)
                    elements.pop(0)
                while len(base_dirs):
                    elements.insert(0, '..')
                    base_dirs.pop(0)
                relative = '/'.join(elements)
                if len(bits.query):
                    relative += '?%s' % (bits.query,)
                # TODO: bits.fragment?
                return relative
            else:
                raise Exception('Don\'t know how to make a path from "%s"' % (url,))
