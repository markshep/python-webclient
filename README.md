# Overview

This is a Python module to simplify interacting with websites.

# Author

Mark Sheppard (<mark [AT] ddf [DOT] net>)

# History

Created: September 2012
